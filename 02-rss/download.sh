#!/usr/bin/env bash

youtube-dl \
  --hls-prefer-ffmpeg \
  --format 43 \
  -x \
  --audio-format m4a \
  --audio-quality 48k \
  --add-metadata \
  --prefer-ffmpeg \
  --embed-thumbnail \
  $@
