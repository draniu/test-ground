#!/usr/bin/env python3

from tkinter import *
import subprocess
import time

def clicked():
    lbl.config(text="  Test GUI")
    btn.destroy()

    i = 10
    while i > 0:
        i -= 1
        autodestroy.config(text="i ulegnie samozniszczeniu za "+str(i+1)+" s")
        window.update()
        time.sleep(1)
    exit (0)

window = Tk()
window.geometry('280x100')
window.title("Pierwsza apka okienkowa")
window.call('wm', 'iconphoto', window._w, PhotoImage(file='./paszczak.png'))


lbl = Label(window, text="   To jest Aplikacja", font=("Arial Bold", 20))
lbl.grid(column=0, row=0)

autodestroy = Label(window)
autodestroy.grid(column=0, row=2)

btn = Button(window, text='Dalej >', command=clicked)
btn.grid(column=0 ,row=1)

window.mainloop()
