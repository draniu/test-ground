#!/usr/bin/env python3
from pprint import pprint
board = [
        [5,3,0,0,7,0,0,0,0],
        [6,0,0,1,9,5,0,0,0],
        [0,9,8,0,0,0,0,6,0],
        [8,0,0,0,6,0,0,0,3],
        [4,0,0,8,0,3,0,0,1],
        [7,0,0,0,2,0,0,0,6],
        [0,6,0,0,0,0,2,8,0],
        [0,0,0,4,1,9,0,0,5],
        [0,0,0,0,8,0,0,7,9]
]

complicated_board = [
        [8,0,0,0,0,0,0,0,0],
        [0,0,3,6,0,0,0,0,0],
        [0,7,0,0,9,0,2,0,0],
        [0,5,0,0,0,7,0,0,0],
        [0,0,0,0,4,5,7,0,0],
        [0,0,0,1,0,0,0,3,0],
        [0,0,1,0,0,0,0,6,8],
        [0,0,8,5,0,0,0,1,0],
        [0,9,0,0,0,0,4,0,0]
]
pprint (board)
def is_possible (x,y,n,board) :
    for i in range(9):
        if board[x][i] == n :
            return False
        if board[i][y] == n :
            return False
    xmin = (x // 3) * 3
    ymin = (y // 3) * 3
    for i in range(xmin, xmin+3):
        for j in range(ymin, ymin+3):
            if board[i][j] ==  n :
                return False
    return True

def solve (board):
    for x in range(9):
        for y in range(9):
            if  board[x][y] == 0 :
                for n in range(1, 10):
                    if is_possible(x,y,n,board):
                        board[x][y] = n
                        solve(board)
                        board[x][y] = 0
                return
    pprint (board)

solve(board)
